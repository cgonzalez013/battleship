'use strict';
var makeMatrix= function () {
	var a= [];
	var lcont= 0;
	var ncont= 1;
	for (var i=0; i< board_setup.rows; i++) {
		a[i]= [];
		for (var j=0; j< board_setup.cols; j++) {
			a[i][j]= board_setup.fill;
			if (a[i][j]== a[0][j] && a[j]== a[0]) {
				a[0][0]= '0'
			}
			if (a[i][j]== a[0][j] && a[j]!= a[0]) {
				a[i][j]= board_setup.abcdario[lcont];
				lcont++;
			};
			if (a[i][j]== a[i][0] && a[i]!= a[0]) {
				a[i][j]= ncont;
				ncont++;
			};
		};
	};
	return a;
};

var board_setup= {
	rows: 10,
	cols: 10,
	abcdario: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
	fill: '~',
	horizontal_head: '<',
	horizontal_tail: '>',
	horizontal_body: '=',
	vertical_head: '^',
	vertical_tail: 'v',
	vertical_body: 'I'
};

var board= { 
};

var action= {
	matrix: makeMatrix(),
	putShip: function (headp, tailp) {
	var matrix= this.matrix
		//correctIfInverse
		if ((headp[0]+ headp[1])> (tailp[0]+ tailp[1])) {
			console.log('the tail is bigger than the head, what a mess! let me fix this...')
			var temp= headp;
			headp= tailp;
			tailp= temp;
		}
		if (matrix[headp[0]][headp[1]]== board_setup.fill && matrix[tailp[0]][tailp[1]]== board_setup.fill) {
			console.log('it appears you know your way around heads and tails')
			//horizontalShip
			if (matrix[headp[0]]== matrix[tailp[0]]) {
				console.log('you have placed a ship horizontally')
				for (var i=(headp[1]+1); i<(tailp[1]); i++) {
							var position_ok= true;
							if (matrix[headp[0]][i]!= board_setup.fill) {
								console.log('it appears you do not know your way around bodies')
								position_ok= false;
							};
				};
				if (position_ok== true) {
					console.log('it appears you do know your way around bodies')
						//putHead
						matrix[headp[0]][headp[1]]= board_setup.horizontal_head;
						//putTail
						matrix[tailp[0]][tailp[1]]= board_setup.horizontal_tail;
						//putHorizontalBody
						for (var i=(headp[1]+1); i<(tailp[1]); i++) {
							matrix[headp[0]][i]= board_setup.horizontal_body;
						};
				};
			};

			//verticalShip
			if (matrix[headp[1]]== matrix[tailp[1]]) {
				console.log('you have placed a vertical ship')
				for (var i=(headp[0]+1); i<(tailp[0]); i++) {
							position_ok= true;
							if (matrix[i][tailp[1]]!= board_setup.fill) {
								console.log('it appears you do not know your way arround bodies')
								position_ok= false;
							};
				};
				if (position_ok== true) {
					console.log('it appears you know your way around bodies')
					//putHead
					matrix[headp[0]][headp[1]]= board_setup.vertical_head;
					//putTail
					matrix[tailp[0]][tailp[1]]= board_setup.vertical_tail;
					//putVerticalBody
						for (var i=(headp[0]+1); i<(tailp[0]); i++) {
							matrix[i][tailp[1]]= board_setup.vertical_body;
						};
				};
			};
		} else {
			return console.log('Posicion no valida, por ahora');
		};
	},
	shoot: function (coordenates) {
		var matrix= this.matrix
		if (matrix[coordenates[0]][coordenates[1]]== board_setup.fill) {
			return console.log('you missed and hit water');
		} else {
			for (var i=0; i<board_setup.abcdario.length; i++) {
				if (board_setup.abcdario[i]== matrix[coordenates[0]][coordenates[1]] || (matrix[coordenates[0]][coordenates[1]]== i) && matrix[coordenates[0]][coordenates[1]]!= 0) {
					 return console.log('poor '+ matrix[coordenates[0]][coordenates[1]]+ '! why must you hurt '+ matrix[coordenates[0]][coordenates[1]]+ ', I think '+ matrix[coordenates[0]][coordenates[1]]+ " wants to be your friend. Why don't you try being a little more friendly? "+ matrix[coordenates[0]][coordenates[1]]+ ' would appreciate it.');
				}
				if (matrix[coordenates[0]][coordenates[1]]== board_setup.horizontal_head || matrix[coordenates[0]][coordenates[1]]== board_setup.horizontal_tail || matrix[coordenates[0]][coordenates[1]]== board_setup.horizontal_body) { 
						matrix[coordenates[0]][coordenates[1]]= 'X';
						return matrix
				}
				if (matrix[coordenates[0]][coordenates[1]]== board_setup.vertical_head || matrix[coordenates[0]][coordenates[1]]== board_setup.vertical_tail || matrix[coordenates[0]][coordenates[1]]== board_setup.vertical_body) {
						matrix[coordenates[0]][coordenates[1]]= 'X';
						return matrix
				} 
			}
				{
					 return console.log('you are really not suppoused to hit that, but you do you kid... you do you.')
				}
		}
	},

	look_at_anemy_board: function (matrix) {
		var enemy_board= []
		for (i= 0; i<matrix.length; i++) {
			enemy_board[i]= []
			for (j= 0; j<matrix[i].length; j++) {
				enemy_board[i][j]= matrix[i][j]
			}
		}

		for (var i= 1; i<enemy_board.length; i++) {
			for (var j= 1; j<enemy_board[i].length; j++) {
				if (enemy_board[i][j]== board_setup.horizontal_head || enemy_board[i][j]== board_setup.horizontal_tail || enemy_board[i][j]== board_setup.horizontal_body) {
					enemy_board[i][j]= '~'
				} else if (enemy_board[i][j]== board_setup.vertical_head || enemy_board[i][j]== board_setup.vertical_tail || enemy_board[i][j]== board_setup.vertical_body) {
					enemy_board[i][j]= '~'
				}
			}
		}
		console.log(enemy_board)
		return enemy_board
	}
};

var matrix= action.matrix
console.log('+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+')
action.putShip([1, 1], [1, 8])
action.shoot([1, 2])
action.shoot([1, 3])
action.look_at_anemy_board(matrix)
console.log(matrix)
//console.log(action.matrix)